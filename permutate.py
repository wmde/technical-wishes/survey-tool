#!/usr/bin/env python3
import fasteners
import random
import re
import pywikibot
import sys

# get, shuffle and store topics
wiki_area = "Wikipedia:Umfragen/Technische_Wünsche_2022_Themenschwerpunkte/"

def main():
    site = pywikibot.Site("de", "wikipedia")
    topics_page = pywikibot.Page(site, wiki_area + "Themen")
    codes_page = pywikibot.Page(site, wiki_area + "Kürzel")
    text = topics_page.text

    i = re.search(r"^\{\{", text, re.M).span()[0]
    for match in re.finditer(r"^\{\{.*", text, re.M):
        pass
    j = match.span()[1]

    header = text[0:i]  # noqa: 203
    footer = text[j:]  # noqa: 203
    topics = text[i:j].split("\n")

    random.shuffle(topics)

    text = header + "\n".join(topics) + footer

    topics_page.text = text
    topics_page.save("Reihenfolge der Themenschwerpunkte permutiert", quiet=True)

    # read shortcodes and titles from page
    parsed_topics = []
    for topic in topics:
        shortcode = re.search(r'\bshortcode\s*=\s*(\w+)\b', topic)
        title = re.search(r'\btitle\s*=\s*(.*?)\s*[{|}]', topic)
        parsed_topics.append((shortcode.group(1), title.group(1)))

    # create wikitext with shortcodes and titles with link to subsection
    codes = []
    code_template = "<code>{shortcode}</code> {title}"

    for topic in parsed_topics:
        shortcode, title = topic
        codes.append(code_template.format(shortcode=shortcode, wiki_area=wiki_area, title=title))

    # write shortcodes to new page
    codes_page.text = "<noinclude>Vorsicht, diese Seite wir automatisch generiert von" \
                    " [https://gitlab.com/wmde/technical-wishes/survey-tool/-/blob/main/permutate.py einem Skript]. " \
                    "Bitte nicht editieren. Inhaltliche Änderungen können gerne " \
                    "[["+ wiki_area +"Themen | hier]] vorgeschlagen werden.\n\n</noinclude>" + \
                    " •\n".join(codes)
    codes_page.save("Reihenfolge der Themenschwerpunkte permutiert", quiet=True)


if __name__ == "__main__":
	lock = fasteners.InterProcessLock("/tmp/permutate.lock")
	if lock.acquire(blocking=False):
		main()
		lock.release()
	else:
		print("Aborting due to active lockfile.", file=sys.stderr)
