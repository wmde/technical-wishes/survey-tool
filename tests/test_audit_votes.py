import audit_votes
import pytest


@pytest.mark.parametrize("text,expected", [
  ("", ""),
  ("abc def", "abc def"),
  ("<s></s>", ""),
  ("<s>abc</s> def", " def"),
  ("<s>abc <s>def</s>", ""),
  ("<s>abc <s>def", "<s>abc <s>def"),
  ("<s>a<foo>bc</s> def", " def"),
  ("<s>ab</s>c <s>def</s>", "c "),
])
def test_strip_strikethrough(text, expected):
  assert expected == audit_votes.strip_strikethrough(text)


@pytest.mark.parametrize("signature,expected", [
  ("", None),
  ("[[Benutzer:Foo|Foo]]", "Foo"),
  ("[[Benutzerin:Foo|Foo]]", "Foo"),
  ("[[Benutzerin:Foo|Foo2]]", "Foo"),
  ("[[Benutzerin:Foo]]", "Foo"),
  ("[[User:Foo]]", "Foo"),
  ("[[User:Foo]] ... [[User:Bar]]", "Foo"),
  ("[[User:Foo|Foo]] ... [[User:Bar]]", "Foo"),
  ("{{unsigniert|Foo|21:53, 1. Feb. 2022}}", "Foo"),
  ("{{unsigniert|Foo}}", "Foo"),
  ("{{unsigniert|Foo}} —[[User:Bar]]", "Foo"),
  ("{{unsigniert|Foo{{broken-thing}} —[[User:Bar]]", "Foo"),
  ("[[Spezial:Beiträge/127.0.0.1|127.0.0.2]]", "127.0.0.2"),
  ("[[Spezial:Beiträge/127.0.0.1|127.0.0.2[[broken]]", "127.0.0.2"),
])
def test_find_signature(signature, expected):
  assert expected == audit_votes.find_signature(signature)


def build_vote(ranking):
	vote = dict(zip(audit_votes.topic_codes, [0] * len(audit_votes.topic_codes)))
	for k, v in ranking.items():
		vote[k] = v
	return vote


@pytest.mark.parametrize("line,expected", [
  ("", {}),
  ("foo", {"ignored": ["not header"]}),
  ("== ==", {"warning": ["empty vote"]}),
  ("== <s>EDIT, WD</s> ==", {"warning": ["empty vote"]}),
  ("== EDIT ==", {"valid": [build_vote({"EDIT": 1})]}),
  ("== EDIT<s>, FOO</s> ==", {"valid": [build_vote({"EDIT": 1})]}),
  ("== WD, EDIT ==", {"valid": [build_vote({"WD": 1, "EDIT": 2})]}),
  ("== WD<s>, FOO</s>, EDIT<s>, BAR</s> ==", {"valid": [build_vote({"WD": 1, "EDIT": 2})]}),
  ("== WD, EDIT, FOO ==", {
    "valid": [build_vote({"WD": 1, "EDIT": 2})],
    "warning": ["invalid code 'FOO'"]}),
  ("== WD, EDIT, MERK, SUCH, INTER, BK ==", {
    "valid": [build_vote({"WD": 1, "EDIT": 2, "MERK": 3, "SUCH": 4, "INTER": 5})],
    "warning": ["more than 5 codes, 'BK'"]}),
])
def test_audit_line(line, expected):
  assert expected == audit_votes.audit_line(line)
