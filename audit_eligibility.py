#!/usr/bin/env python3
#
# Usage:
#   ./audit-eligibility
#
# Results are written to the directory `results-2022`
#
from bs4 import BeautifulSoup
import csv
from datetime import datetime
import fasteners
import json
import os
import pywikibot
import sys
import urllib.parse
import urllib.request

diff_url = "https://de.wikipedia.org/wiki/Special:Diff/{revid}"
basepage = "Wikipedia:Umfragen/Technische_W%C3%BCnsche_2022_Themenschwerpunkte/Stimmabgaben"

resultsdir = "results-2022"
eligible_file = resultsdir + "/eligible-editors.csv"
ineligible_file = resultsdir + "/ineligible-editors.csv"
user_cache_file = resultsdir + "/_user_cache.json"

# Current timestamp with hour resolution.
this_run = datetime.now().isoformat()[:13]

# using new eligibility ruleset at our test server
eligibility_tool = "https://wmde-reference-previews.toolforge.org/accounteligibility/59/{username}?wiki=dewiki"


def check_user_eligibility(user):
	# TODO: tweak eligibility tool to provide a machine-readable endpoint.
	url = eligibility_tool.format(username=urllib.parse.quote(user.replace(" ", "_")))
	response = urllib.request.urlopen(url)
	soup = BeautifulSoup(response.read(), 'html.parser')
	return len(soup.findAll('div', {'class': 'success'})) > 0


def write_user_cache(user_cache):
	with open(user_cache_file, "w") as f:
		f.write(json.dumps(user_cache, indent=4))


def read_user_cache():
	try:
		with open(user_cache_file, "r") as f:
			return json.loads(f.read())
	except:
		print("No user cache, recreating from scratch.", file=sys.stderr)
		return {}


def user_first_seen(anon, name):
	return {
		"anon": anon,
		"revs": [],
		"name": name,
		"first_seen_run": this_run,
		# Note: will not reevaluate eligibility so e.g. edit
		# count is frozen, and users appearing later
		# may have an unfair advantage.
		"eligible": False if anon else check_user_eligibility(name)
    }


def write_users(path, users):
	with open(path, "w") as f:
		writer = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
		writer.writerow(["first_seen", "user_name", "edits..."])

		for user in users:
			normalized = [
				user["first_seen_run"],
				user["name"]
			]
			normalized.extend(
				map(lambda revid: diff_url.format(revid=revid), user["revs"]))
			
			writer.writerow(normalized)


def write_reports(user_cache):
	eligible = []
	ineligible = []

	for user in user_cache.values():
		if user["eligible"]:
			eligible.append(user)
		else:
			ineligible.append(user)

	write_users(eligible_file, eligible)
	write_users(ineligible_file, ineligible)

	print("Found {e} eligible users and {i} ineligible users.".format(
			e=len(eligible),
			i=len(ineligible)))


def main():
	site = pywikibot.Site("de", "wikipedia")
	page = pywikibot.Page(site, basepage)

	user_cache = read_user_cache()

	for rev in page.revisions():
		user = user_cache[rev.user] \
			if rev.user in user_cache \
			else user_first_seen(rev.anon, rev.user)

		if rev["revid"] not in user["revs"]:
			user["revs"].append(rev["revid"])

		user_cache[user["name"]] = user

	os.makedirs(resultsdir, exist_ok=True)
	write_user_cache(user_cache)
	write_reports(user_cache)


if __name__ == "__main__":
	lock = fasteners.InterProcessLock("/tmp/audit-eligibility.lock")
	if lock.acquire(blocking=False):
		main()
		lock.release()
	else:
		print("Aborting due to active lockfile.", file=sys.stderr)
