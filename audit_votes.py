#!/usr/bin/env python3

import collections
import csv
from datetime import datetime
import fasteners
import pywikibot
import re
import sys

voting_page = "Wikipedia:Umfragen/Technische_W%C3%BCnsche_2022_Themenschwerpunkte/Stimmabgaben"
voting_page_revid = None
# Lock to a specific revision,
# voting_page_revid = 12345

create_named_ballot_file = False

resultsdir = "results-2022"
tallyfile = resultsdir + "/tally.csv"
ignorefile = resultsdir + "/not-processed.csv"
warningfile = resultsdir + "/ballot-errors.csv"
ballotfile = resultsdir + "/named-ballots.csv"

# TODO: Update with actual codes
topic_codes = {
	"EDIT",
	"WD",
	"MERK",
	"SUCH",
	"INTER",
	"BK",
	"EINZ",
	"VERS",
	"VORL",
	"UPL",
	"FORM",
	"MEDIA",
	"BEO",
	"VE",
	"MOB",
	"KAT",
}

this_run = datetime.now().isoformat()[:13]


def strip_strikethrough(text):
	return re.sub(r'<s>.*?</s>', '', text)


def find_signature(content):
	if not content:
		return None

	# Extract the first signature or unregistered user contributions link.
	# TODO: Calculate matching user namespace prefixes accounting for the site's
	# content language.
	# FIXME: This could fail in a way that grabs the wrong username from a block,
	# but this might require a sophisticated fix such as looking for the first
	# edit in a section.
	result = re.search(r'''(?x)
		{{\s*unsigniert
		(?:\|(?P<unsigned_user>[^|{}]+))?
		|
		\[\[
		(?:
			(?:Benutz\w*|User)
			:
			|
			(?:Spezial:Beiträge|Special:Contributions)
			(?:/[^|[\]]+)?
			\|
		)
		(?P<user>[^|[\]]+)
	''', content)
	if result:
		return result.group("unsigned_user") or result.group("user") or None
	else:
		return None


def write_job_header(file, page):
	file.writelines([
		"Job timestamp," + this_run + "\n",
		"Permalink," + page.permalink(oldid=voting_page_revid, with_protocol=True) + "\n"
	])


def audit_line(line):
	if not line:
		return {}

	matches = re.findall(r'^==(.*?)==$', line)
	if len(matches) == 0:
		return {"ignored": ["not header"]}

	line = strip_strikethrough(matches[0]).strip()

	codes = re.split(r'\s*[, ]\s*', line)
	if codes == ['']:
		codes = []

	vote = dict(zip(topic_codes, [0] * len(topic_codes)))
	rank = 1

	result = collections.defaultdict(list)
	for code in codes:
		if rank > 5:
			result["warning"].append(
				"more than 5 codes, '{}'".format(",".join(codes[5:])))
			break

		code = code.upper()
		if code not in topic_codes:
			result["warning"].append("invalid code '{}'".format(code))
			continue
		if vote[code] > 0:
			result["warning"].append("duplicate code '{}'".format(code))
			continue

		vote[code] = rank
		rank += 1
	
	if sum(vote.values()) == 0:
		result["warning"].append("empty vote")
	else:
		result["valid"].append(vote)

	return result


def audit_all_votes(wikitext):
	output = collections.defaultdict(list)

	# Line-based tests
	lines = wikitext.splitlines()
	for index, line in enumerate(lines):
		line_num = index + 1
		line = line.strip()

		result = audit_line(line)
		
		# TODO: Merge logic could be cleaner.
		for key in ["warning", "ignored"]:
			if key in result:
				for message in result[key]:
					output[key].append((line_num, message, line))

		if "valid" in result:
			output["valid"].extend(result["valid"])
	
	# Naively associate usernames -- FIXME: the last entry gets clipped
	matches = re.finditer(r'^\s*==\s*(?P<header>.*?)\s*==\s*$(?P<body>.*?)(?=^\s*==|\Z)',
		wikitext, flags=(re.DOTALL | re.MULTILINE))
	for match in matches:
		header_offset = match.start("header")
		header_line = wikitext[:header_offset].count("\n") + 1
		user = find_signature(match.group("body"))
		if not user:
			output["warning"].append((header_line, "no signature", match.group("body")))
		else:
			stripped_vote = strip_strikethrough(match.group("header"))
			output["named_ballots"].append((header_line, user, stripped_vote))

	return output


def write_reports(page, output):
	# Write reports
	with open(tallyfile, "w") as f:
		write_job_header(f, page)
		# FIXME: column order is scrambled every time
		writer = csv.DictWriter(f, fieldnames=topic_codes)
		writer.writeheader()
		writer.writerows(output["valid"])

	output["ignored"].sort(key=lambda l: l[0])
	with open(ignorefile, "w") as f:
		write_job_header(f, page)
		writer = csv.writer(f)
		writer.writerow(["line", "reason", "text"])
		writer.writerows(output["ignored"])

	output["warning"].sort(key=lambda l: l[0])
	with open(warningfile, "w") as f:
		write_job_header(f, page)
		writer = csv.writer(f)
		writer.writerow(["line", "reason", "text"])
		writer.writerows(output["warning"])

	if create_named_ballot_file:
		# Don't publish this file on the Internet.
		with open(ballotfile, "w") as f:
			write_job_header(f, page)
			writer = csv.writer(f)
			writer.writerow(["line", "username", "ballot"])
			writer.writerows(output["named_ballots"])


def main():
	global voting_page_revid

	site = pywikibot.Site("de", "wikipedia")
	page = pywikibot.Page(site, voting_page)

	if voting_page_revid is None:
		voting_page_revid = page.latest_revision_id
	wikitext = page.getOldVersion(voting_page_revid)

	output = audit_all_votes(wikitext)
	write_reports(page, output)


if __name__ == "__main__":
	lock = fasteners.InterProcessLock("/tmp/audit-votes.lock")
	if lock.acquire(blocking=False):
		main()
		lock.release()
	else:
		print("Aborting due to active lockfile.", file=sys.stderr)
